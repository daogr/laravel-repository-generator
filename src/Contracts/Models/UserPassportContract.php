<?php

    namespace Otodev\Contracts\Models;

    use Illuminate\Contracts\Auth\Authenticatable as UserContract;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\HasOne;

    /**
     * Interface UserPassportContract
     * @package Otodev\Contracts\Models
     */
    interface UserPassportContract {

        /**
         * Find the user instance for the given username.
         *
         * @param string $username
         *
         * @return Builder|Model|object|null
         */
        public function findForPassport(string $username);

        /**
         * Get the users's full name.
         *
         * @return string
         */
        public function getNameAttribute();

        /**
         * Role
         * @return HasOne
         */
        public function role();

        /**
         * Retrieve a user by the given credentials.
         *
         * @param Builder $query
         * @param array   $credentials
         *
         * @return Model|object|static|null|bool
         */
        public function scopeRetrieveByCredentials($query, array $credentials);

        /**
         * Attempt to authenticate a user using the given credentials.
         *
         * @param Builder $query
         * @param array   $credentials
         * @param bool    $remember
         *
         * @return bool
         */
        public function scopeAttempt($query, array $credentials = [], $remember = false);

        /**
         * Validate a user against the given credentials.
         *
         * @param UserContract $user
         * @param array        $credentials
         *
         * @return bool
         */
        public function validateCredentials(UserContract $user, array $credentials);

        /**
         * Get the first key from the credential array.
         *
         * @param array $credentials
         *
         * @return string|null
         */
        public function firstCredentialKey(array $credentials);

        /**
         * Determine if the user matches the credentials.
         *
         * @param mixed $user
         * @param array $credentials
         *
         * @return bool
         */
        public function hasValidCredentials($user, $credentials);
    }
