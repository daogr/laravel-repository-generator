<?php

    namespace Otodev\Utils;

    /**
     * Class ResponseUtil
     * @package Otodev\Utils
     */
    class ResponseUtil {
        const RESPONSE = ['success', 'data', 'message'];
        const SUCCESS  = ['success', 'message', 'code'];
        const ERROR    = ['success', 'data', 'message'];

        /**
         * @param     $message
         * @param     $data
         * @param int $code
         *
         * @return array
         */
        public static function response($message, $data, $code = 200) {
            return ['success' => true, 'data' => $data, 'code' => $code, 'message' => $message];
        }

        /**
         * @param       $message
         * @param array $data
         * @param int   $code
         *
         * @return array
         */
        public static function error($message, array $data = [], $code = 422) {
            return ['success' => false, 'data' => $data, 'code' => $code, 'message' => $message];
        }

        /**
         * @param     $message
         * @param int $code
         *
         * @return array
         */
        public static function success($message, $code = 200) {
            return ['success' => true, 'message' => $message, 'code' => $code];
        }
    }
