<?php

	namespace Otodev\Console\Commands;

    use Illuminate\Console\GeneratorCommand;
    use Illuminate\Contracts\Filesystem\FileNotFoundException;
    use Symfony\Component\Console\Input\InputArgument;

    class ApiResourceMakeCommand extends GeneratorCommand {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $name = 'make:resource:api';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Create a new api resource class';

        /**
         * Execute the console command.
         *
         * @return bool|null
         *
         * @throws FileNotFoundException
         */
        public function handle() {
            $name = $this->qualifyClass($this->getNameInput());

            $path = $this->getPath("{$name}Resource");

            // First we will check to see if the class already exists. If it does, we don't want
            // to create the class and overwrite the user's code. So, we will bail out so the
            // code is untouched. Otherwise, we will continue generating this class' files.
            if((!$this->hasOption('force') ||
                    !$this->option('force')) &&
                $this->alreadyExists($this->getNameInput())) {
                $this->error($this->type . ' already exists!');

                return false;
            }

            // Next, we will generate the path to the location where this class' file should get
            // written. Then, we will build the class and make the proper replacements on the
            // stub files so that it gets the correctly formatted namespace and class name.
            $this->makeDirectory($path);

            $this->files->put($path, $this->sortImports($this->buildClass($name)));

            $this->info($this->type . ' created successfully.');
        }

        /**
         * Get the stub file for the generator.
         *
         * @return string
         */
        protected function getStub() {
            return __DIR__ . '/stubs/api_resource.stub';
        }

        /**
         * Get the default namespace for the class.
         *
         * @param string $rootNamespace
         *
         * @return string
         */
        protected function getDefaultNamespace($rootNamespace) {
            return $rootNamespace . '\Http\Resources';
        }

        /**
         * Get the console command arguments.
         *
         * @return array
         */
        protected function getArguments() {
            return [
                ['name', InputArgument::REQUIRED, 'The name of the class'],
                ['type', InputArgument::REQUIRED, 'The type of the class'],
            ];
        }

        /**
         * Get the desired class type from the input.
         *
         * @return string
         */
        protected function getTypeInput() {
            return trim($this->argument('type'));
        }

        /**
         * Get the desired class name from the input.
         *
         * @return string
         */
        protected function getNameInput() {
            $name = trim($this->argument('name'));
            $type = $this->getTypeInput();

            $this->type = "Api request {$type} class";

            if($type == 'create') return "{$name}Store";
            if($type == 'update') return "{$name}Update";

            return $name;
        }
    }
